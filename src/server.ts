import * as express from 'express';
import * as bodyParser from 'body-parser';
import {graphqlExpress, graphiqlExpress} from 'graphql-server-express';
import {makeExecutableSchema} from 'graphql-tools';
import {typeDefs} from './typeDefs'


const PORT = 3000;

var app = express();

const Schema = makeExecutableSchema({
  typeDefs: typeDefs
})

app.use('/graphql', bodyParser.json(), graphqlExpress({ schema: Schema }));

app.use('/graphiql', graphiqlExpress({
  endpointURL: '/graphql',
}));

app.listen(PORT);
